FROM centos:7.3.1611

# Install RPM Build
RUN yum -y update && \
    yum install -y rpm-build

# Setup Directories
RUN mkdir -p /root/rpmbuild/{RPMS,SRPMS,BUILD,SOURCES,SPECS,tmp}

# Add the build script
ADD ./build-rpm.sh /usr/local/bin/build-rpm.sh
RUN chmod +x /usr/local/bin/build-rpm.sh

VOLUME ["/build"]

WORKDIR /build


CMD ["/usr/local/bin/build-rpm.sh"]


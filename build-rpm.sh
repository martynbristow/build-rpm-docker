#!/bin/bash
set -euo pipefail

DEBUG=""
: ${USER:=0}
: ${GROUP:=0}
function log_info {
    if [ ! -z "${DEBUG}" ]; then echo "$1"; fi
}
function log_error {
    printf "[31m\$1[0m\\n"
}

for spec in $(ls *.spec) ; do
# Install Dependancies
    yum-builddep -y --nogpgcheck $spec
done

TOPDIR="${HOME}/rpmbuild"

# copy sources and spec into rpmbuild's work dir
log_info "Copying sources into: ${TOPDIR}/SOURCES/"
cp -r * "${TOPDIR}/SOURCES/"

for spec in *.spec ; do
    cp "${spec}" "${TOPDIR}/SPECS/"
    spec="${TOPDIR}/SPECS/${spec##*/}"
    # Build the RPMs
    log_info "Building ${spec}"
    rpmbuild -ba "${spec}"
done

# Copy back the artifact
cp /root/rpmbuild/RPMS/*/*.rpm /build